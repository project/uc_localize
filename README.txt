
-- SUMMARY --

A quick fix for internationalization issues on Ubercart managed multi-language e-commerce sites.

**********************************************************************************
                 WARNING
This module is not a permanent solution, and serves only as a quick fix for sites using
Ubercart that need to translate the basic ui elements on their site without jumping through
a bunch of hoops.

It is also my hope that the module can serve as a case-study and proof-of-concept
to support the one product = one node (indeed, one node -> many translations) model
as opposed to the one translation = one node model, which I believe is logically flawed.

Please at least read CONFIGURATION OF YOUR SITE FOR THIS MODULE.

**********************************************************************************

See http://www.ubercart.org/forum/internationalization/10878/i18n_issues_i_d6uc2_multilingual_sites
for an in depth discussion on this issue.

Author: Aaron Craig (Evolving Design, info@evolving-design.com)


-- INSTALLATION --
The normal method

-- USAGE --
The module adds new fields in various places so that you can translate those strings.  The strings are kept in a
separate table and are injected into HTML output where appropriate.

The system looks at active languages and adds new fields for non-default languages.


-- CAVEATS --
CHANGING YOUR DEFAULT LANGUAGE AFTER TRANSLATING STRINGS MAY REQUIRE YOU TO TRANSLATE THE STRINGS AGAIN

The translated values DO NOT appear in the administration forms, except as the value in the text field when actually doing the translation.

As this is still a work in progress, there may be areas in the public facing pages and forms where the untranslated text appears.  Ubercart has many forms and views, and one reason for releasing this module so early is so that others can help me find the areas that need attention.

If you see untranslated text showing up where it shouldn't, please create an issue on the Drupal.org project page (http://drupal.org/project/uc_localize)

-- CONFIGURATION OF YOUR SITE FOR THIS MODULE --
The module expects your links to catalog categories to contain the string 'catalog/' in them.

-- CONTACT --
Aaron Craig (Evolving Design, info@evolving-design.com, http://drupal.org/user/118575)
